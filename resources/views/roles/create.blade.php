@extends('layouts.app')
@section('title')
    <title>Add Role</title>
@endsection
@section('cssAdded')
    <style>
        .table-borderless > tbody > tr > td,
        .table-borderless > tbody > tr > th,
        .table-borderless > tfoot > tr > td,
        .table-borderless > tfoot > tr > th,
        .table-borderless > thead > tr > td,
        .table-borderless > thead > tr > th {
            border: none;
        }
    </style>
@endsection
@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h2 class="m-0 text-dark">Roles Management</h2>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('roles.index') }}">Roles</a></li>
                    <li class="breadcrumb-item active">Add Role</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<section class="container-fluid">
    <div class="card">
        @include ('includes.flash')
        <div class="card-body">
            <form role="form" method="post" action="{{ route('roles.store') }}">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputPassword1"><b>Role Name</b></label>
                        <input type="text" class="form-control" id="name" name="name" id="exampleInputPassword1" placeholder="Role Name" required>
                    </div>
                    <br>
                    <h4>Permissions</h4>
                    <div class="custom-control custom-checkbox small">                        
                        <table class="table table-borderless">
                            @foreach ($permissions as $key => $permission)
                                <tr>
                                    <td>
                                        <input type="checkbox" class="custom-control-input parentPermission" data-kunci="{{ $key }}" id="{{ $key }}" value="{{ $key }}">
                                        <label class="custom-control-label" for="{{ $key }}" style="padding-top:3px;"><b>{{ ucwords($key) }} Module</b></label>
                                    </td>
                                    @foreach ($permission as $key2 => $item)
                                    <td>
                                        <input type="checkbox" name="permissions[]" class="custom-control-input" id="{{ $item->name }}" value="{{ $item->id }}">
                                        <label class="custom-control-label" for="{{ $item->name }}" style="padding-top:3px;">{{ ucwords(explode('-', $item->name)[1]) }}</label>
                                    </td>
                                    @endforeach
                                </tr>
                            @endforeach                            
                        </table>                                                                                                                        
                    </div>
                </div>
                <div class="card-footer">
                    <a href="{{ route('roles.index') }}" class="btn btn-warning">Back</a>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</section>
@include ('includes.script')
@section('jsAdded')
    <script>
        let kunci = null;
        let isChecked = null;
        let create = null;
        let list = null;
        let edit = null;
        let del = null;
        $(document).on('click','.parentPermission',function(){
            kunci = $(this).data('kunci');
            isChecked= $('#' + kunci).is(":checked");
            create = kunci+'-create';
            list = kunci+'-list';
            edit = kunci+'-edit';
            del = kunci+'-delete';
            if (isChecked == true) {
                $('#' + create).prop("checked", true );
                $('#' + list).prop("checked", true );
                $('#' + edit).prop("checked", true );
                $('#' + del).prop("checked", true );
            }else {
                $('#' + create).prop("checked", false );
                $('#' + list).prop("checked", false );
                $('#' + edit).prop("checked", false );
                $('#' + del).prop("checked", false );
            }
        });

    </script>
@endsection
@endsection
