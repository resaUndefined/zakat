@extends('layouts.app')

@section('title')
    <title>Roles Page</title>
@endsection

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h2 class="m-0 text-dark">Roles Management</h2>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item active">Roles</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<section class="container-fluid">
    <div class="card">
        @include ('includes.flash')
        <div class="card-body">
            <table id="data-admin" class="table table-bordered table-striped">
                <thead>
                    <tr>
                    <th width="40">NO</th>
                    <th>ROLE NAME</th>                    
                    <th width="80">AKSI</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($roles as $key => $role)
                        <tr>
                            <td class="text-center">{{$loop->iteration}}</td>
                            <td>{{ $role->name }}</td>                        
                            <td class="text-center">
                                @can('role-edit')
                                    <a href="/roles/{{$role->id}}/edit">
                                        <button class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i></button>
                                    </a>
                                @endcan
                                @can('role-delete')
                                    <button type="button" class="btn btn-danger btnDelete" data-nama="{{ $role->name }}" data-id="{{ $role->id }}" data-toggle="tooltip" data-placement="top" title="Delete">
                                        <i class="fa fa-trash"></i>
                                    </button>                            
                                @endcan
                            </td>
                        </tr>                        
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>
    @include('includes.modals_delete')
    @include ('includes.script')
    @can('role-create')
        <script type="text/javascript">
            $(document).ready(function(){
                $("#data-admin_length").append('<a  href="{{ route('roles.create') }}"> <button type="button" class="btn btn-outline-primary ml-3">Add Role</button></a>');
            });
        </script>
    @endcan
    @section('jsAdded')
        <script>
            let id = null;
            let url = null;
            let msg = null;
            let name = null;

            $(document).on('click','.btnDelete',function(){
                id = $(this).data('id');
                name = $(this).data('nama');
                url = "{{ route('roles.destroy', '') }}"+"/"+id;
                msg = "Are you sure to delete this role ("+name+") ?";
                $('#formDelete').attr('action', url);            
                $('#modalContent').empty();
                $('#modalContent').append(msg);
                $('#modalDelete').show();
            });

            $(document).on('click','.btnCancel',function(){            
                $('#modalDelete').hide();
            });
        </script>
    @endsection
@endsection
