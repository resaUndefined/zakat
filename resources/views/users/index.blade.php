@extends('layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h2 class="m-0 text-dark">Users Management</h2>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                        <li class="breadcrumb-item active">Users List</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <section class="container-fluid">
        <div class="card">
            @include ('includes.flash')
            <div class="card-body">
                <table id="data-admin" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                        <th width="40">NO</th>
                        <th>NAMA</th>
                        <th>EMAIL</th>
                        @can('user-edit')
                            <th>ACTIVITY</th>
                            <th>STATUS</th>
                        @endcan
                        <th>ROLE</th>
                        <th width="80">AKSI</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $key => $user)
                        <tr>
                            <td class="text-center">{{$loop->iteration}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            @can('user-edit')
                                <td>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" style="cursor: pointer;" class="custom-control-input btnForceLogout" data-id="{{ $user->id }}" data-name="{{ $user->name }}" id="userActivity-{{ $user->id }}" @if ($user->logout == false)
                                            checked
                                        @else
                                            disabled
                                        @endif>
                                        <label class="custom-control-label" style="cursor: pointer;" for="userActivity-{{ $user->id }}">
                                            @if ($user->logout == false)
                                                Login
                                            @else
                                                Logout
                                            @endif
                                        </label>
                                    </div>    
                                </td>
                                <td>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" style="cursor: pointer;" class="custom-control-input btnChangeStatus" data-id="{{ $user->id }}" data-name="{{ $user->name }}" id="userStatus-{{ $user->id }}" @if ($user->is_active)
                                            checked                                        
                                        @endif>
                                        <label class="custom-control-label" style="cursor: pointer;" for="userStatus-{{ $user->id }}">@if ($user->is_active)
                                            Active
                                        @else
                                            Deactive
                                        @endif</label>                                        
                                    </div>    
                                </td>
                            @endcan
                            <td>
                                @if(!empty($user->getRoleNames()))
                                @foreach($user->getRoleNames() as $v)
                                    <label class="badge badge-success">{{ $v }}</label>
                                @endforeach
                                @endif
                            </td>
                            <td class="text-center">
                                @can('user-edit')
                                    <a href="/users/{{$user->id}}/edit">
                                        <button class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i></button>
                                    </a>
                                @endcan
                                @can('user-delete')
                                    <button type="button" class="btn btn-danger btnDelete" data-nama="{{ $user->name }}" data-id="{{ $user->id }}" data-toggle="tooltip" data-placement="top" title="Delete">
                                        <i class="fa fa-trash"></i>
                                    </button>                            
                                @endcan
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    @include('includes.modals_delete')
    @include('includes.modal_force_logout')
    @include('includes.modal_change_status')
    @include ('includes.script')
    @can('user-create')
        <script type="text/javascript">
            $(document).ready(function(){
                $("#data-admin_length").append('<a  href="{{ route('users.create') }}"> <button type="button" class="btn btn-outline-primary ml-3">Add User</button></a>');
            });
        </script>
    @endcan
    @section('jsAdded')
        <script>
            let id = null;
            let url = null;
            let msg = null;
            let name = null;
            let isChecked = null;
            let status = 0;
            let idUser = null;
            $(document).on('click','.btnDelete',function(){
                id = $(this).data('id');
                name = $(this).data('nama');
                url = "{{ route('users.destroy', '') }}"+"/"+id;
                msg = "Are you sure to delete this user ("+name+") ?";
                $('#formDelete').attr('action', url);                            
                $('#modalContent').empty();
                $('#modalContent').append(msg);
                $('#modalDelete').show();
            });

            $(document).on('click','.btnForceLogout',function(){                
                id = $(this).data('id');
                name = $(this).data('nama');
                idUser = '#userActivity-'+id;
                isChecked = $('#userActivity-' + id).is(":checked");                
                url = "{{ route('forceLogout', '') }}"+"/"+id;
                msg = "Are you sure to logout this user ("+name+") ?";
                $('#formForceLogout').attr('action', url);                            
                $('#modalContentLogout').empty();
                $('#modalContentLogout').append(msg);
                $('#modalForceLogout').show();
            });
            
            $(document).on('click','.btnChangeStatus',function(){                
                id = $(this).data('id');
                name = $(this).data('nama');
                idUser = '#userStatus-'+id;
                isChecked = $('#userStatus-' + id).is(":checked");                
                if (isChecked == true) {
                    status = 1;   
                    msg = "Are you sure to activated this user ("+name+") ?";
                }else {
                    status = 0;
                    msg = "Are you sure to deactivated this user ("+name+") ?";
                }
                url = "{{ route('changeStatus', '') }}"+"/"+id;                
                $('#formChangeStatus').attr('action', url);                            
                $('#modalContentChangeStatus').empty();
                $('#modalContentChangeStatus').append(msg);
                $('#modalChangeStatus').show();
            });

            $(document).on('click','.btnCancel',function(){            
                $('#modalDelete').hide();
            });

            $(document).on('click','.btnCancel2',function(){  
                if (isChecked == true) {
                    $(idUser).prop("checked", false );
                } else {
                    $(idUser).prop("checked", true );
                }
                $('#modalForceLogout').hide();
            });

            $(document).on('click','.btnCancel3',function(){            
                if (isChecked == true) {
                    $(idUser).prop("checked", false );
                } else {
                    $(idUser).prop("checked", true );
                }
                $('#modalChangeStatus').hide();
            });
        </script>
    @endsection
@endsection
