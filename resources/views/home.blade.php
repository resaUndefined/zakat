@extends('layouts.app')
@section('title')
    <title>Dashboard</title>
@endsection
@section('cssAdded')
<style>
    a {
      text-decoration:none;
   }
</style>
@endsection
@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>        
    </div>

    <!-- Content Row -->
    <div class="row">
        @if ($roleName == 'Amil Zakat')
            @foreach ($data as $key => $amil)
                @if ($key % 2 == 0)
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            {{ $amil->name }}</div>
                                        <div class="h6 mb-0 font-weight-bold text-gray-800">
                                            Muzaki : {{ count($amil->zakats) }} orang
                                        </div>                                                        
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-mosque fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-success shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            {{ $amil->name }}</div>
                                        <div class="h6 mb-0 font-weight-bold text-gray-800">
                                            Muzaki : {{ count($amil->zakats) }} orang
                                        </div>                                                        
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-mosque fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif            
            @endforeach
        @else
            @foreach ($data as $key => $amil)
                @if ($key % 2 == 0)
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            <a href="{{ route('zakat-fitrah.peronda',$amil->id) }}" style="text-decoration:none;">
                                                {{ $amil->name }}
                                            </a>
                                        </div>
                                        <div class="h6 mb-0 font-weight-bold text-gray-800">
                                            <a href="{{ route('zakat-fitrah.peronda',$amil->id) }}" style="text-decoration:none;color:#5a5c69!important;">
                                                Muzaki : {{ count($amil->zakats) }} orang
                                            </a>                                            
                                        </div>                                                        
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-mosque fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-success shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            <a href="{{ route('zakat-fitrah.peronda',$amil->id) }}" style="text-decoration:none;">
                                                {{ $amil->name }}
                                            </a>    
                                        </div>
                                        <div class="h6 mb-0 font-weight-bold text-gray-800">
                                            <a href="{{ route('zakat-fitrah.peronda',$amil->id) }}" style="text-decoration:none;color:#5a5c69!important;">
                                                Muzaki : {{ count($amil->zakats) }} orang
                                            </a>                                            
                                        </div>                                                        
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-mosque fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif            
            @endforeach
        @endif                        
    </div>
    <!-- Content Row -->
    {{-- statistic --}}
    <div class="row">

        <!-- Content Column -->
        <div class="col-lg-12 mb-4">

            <!-- Project Card Example -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Statistika</h6>
                </div>
                <div class="card-body">
                    <h4 class="small font-weight-bold">Total Muzaki Beras ({{ $zakatBeras }} orang) <span class="float-right">{{ $berasPercentage }}%</span></h4>
                    <div class="progress mb-4">
                        <div class="progress-bar bg-success" role="progressbar" style="width: {{ $berasPercentage }}%" aria-valuenow="{{ $berasPercentage }}" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <h4 class="small font-weight-bold">Total Muzaki Uang ({{ $zakatUang }} orang) <span class="float-right">{{ $uangPercentage }}%</span></h4>
                    <div class="progress mb-4">
                        <div class="progress-bar bg-info" role="progressbar" style="width: {{ $uangPercentage }}%" aria-valuenow="{{ $uangPercentage }}" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>                    
                </div>
            </div>
        </div>       
    </div>
    {{-- end statistic --}}
    <div class="row">
        <div class="col-xl-6 col-md-4 mb-4">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                Total Beras
                            </div>
                            <div class="h6 mb-0 font-weight-bold text-gray-800">
                                {{ $totalBeras }} Kg
                            </div>                                                        
                        </div>
                        <div class="col-auto">
                            <i class="fa fa-pagelines fa-2x text-gray-300"></i>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-6 col-md-4 mb-4">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                Total Uang yang sudah dibelikan beras
                            </div>
                            <div class="h6 mb-0 font-weight-bold text-gray-800">
                                @currency($totalUangBeras) / <span class="text-gray-600"> ({{ $konversiUang }} kg beras)</span>
                            </div>                                                        
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-money fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>                                      
        <div class="col-xl-6 col-md-4 mb-4">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                Total Uang yang belum dibelikan beras
                            </div>
                            <div class="h6 mb-0 font-weight-bold text-gray-800">
                                @currency($totalUang)
                            </div>                                                        
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-money fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>                                      
        <div class="col-xl-6 col-md-4 mb-4">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                Total Shodaqoh
                            </div>
                            <div class="h6 mb-0 font-weight-bold text-gray-800">
                                @currency($totalShodaqoh)
                            </div>                                                        
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-money fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>                              
    </div>
</div>
@include ('includes.script')
@endsection
