@extends('layouts.app')

@section('title')
    <title>Halaman Zakat</title>
@endsection
@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h2 class="m-0 text-dark">Manajemen Zakat <small><span class="badge badge-pill badge-info">{{ $amil->name }}</span></small></h2>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item active">Manajemen Zakat {{ $amil->name }}</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<section class="container-fluid">    
    @include ('includes.flash')
    <div class="card shadow mb-4">
        <div class="card-body">  
            <div class="table-responsive">
                <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
                    <table id="data-admin" class="table table-bordered dataTable table-striped">
                        <thead>
                            <tr>
                                <th width="40">No</th>
                                <th>Tanggal</th>           
                                <th>Muzaki</th>           
                                <th>Beras</th>                    
                                <th>Uang</th> 
                                <th>Sudah Dibelikan</th>                            
                                <th>Shodaqoh</th>          
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $key => $an)
                                <tr>
                                    <td class="text-center">{{$loop->iteration}}</td>
                                    <td>{{ \Carbon\Carbon::parse($an->tanggal)->format("l, d F Y") }}</td>                                                        
                                    <td>
                                        {{ $an->nama }}
                                    </td>                                                            
                                    <td>
                                        @if ($an->jumlah_beras != null)
                                            {{ $an->jumlah_beras }} Kg
                                        @else
                                            -
                                        @endif
                                    </td>                        
                                    <td>
                                        @if ($an->jumlah_uang != null)
                                            @currency($an->jumlah_uang)
                                        @else
                                            -
                                        @endif
                                    </td>   
                                    <td>
                                        @if ($an->jenis == 'beras')
                                            -
                                        @else
                                            @if ($an->sudah_beli == '1')
                                                <i class="fa fa-check" style="color:cornflowerblue;"></i> Sudah
                                            @else
                                                <i class="fa fa-times" style="color:firebrick;"></i> Belum
                                            @endif
                                        @endif    
                                    </td> 
                                    <td>
                                        @if ($an->sisa_uang != null)
                                            @currency($an->sisa_uang)
                                        @else
                                            -
                                        @endif
                                    </td>                                                                                             
                                </tr>                        
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
    @include('includes.modals_delete')
    @include ('includes.script')
    <script type="text/javascript">
        $(document).ready(function(){
            $("#data-admin_length").append('<a  href="{{ route('zakat-fitrah.index') }}"> <button type="button" class="btn btn-outline-warning ml-3"><i class="fa fa-back"></i> Kembali</button></a>');
        });
    </script>
    @section('jsAdded')
        <script>
            let id = null;
            let url = null;
            let msg = null;
            let name = null;

            $(document).on('click','.btnDelete',function(){
                id = $(this).data('id');
                name = $(this).data('nama');
                url = "{{ route('zakat-fitrah.destroy', '') }}"+"/"+id;
                msg = "Apakah kamu yakin ingin menghapus zakat atas nama ("+name+") ?";
                $('#formDelete').attr('action', url);            
                $('#modalContent').empty();
                $('#modalContent').append(msg);
                $('#modalDelete').show();
            });

            $(document).on('click','.btnCancel',function(){            
                $('#modalDelete').hide();
            });
        </script>
    @endsection
@endsection
