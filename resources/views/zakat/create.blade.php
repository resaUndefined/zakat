@extends('layouts.app')

@section('title')
    <title>Halaman Zakat</title>
@endsection
@section('cssAdded')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />    
    <style>
        .required:after {
            content:" *";
            color: red;
        }        
        .required2:before {
            content:" *";
            color: red;
        }        
    </style>
@endsection
@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h2 class="m-0 text-dark">Manajemen Zakat Fitrah</h2>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('zakat-fitrah.index') }}">Zakat Fitrah</a></li>
                    <li class="breadcrumb-item active">Tambah Zakat Fitrah</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<section class="container-fluid">
    <div class="card">
        @include ('includes.flash')
        <div class="card-body">
            <form role="form" method="post" action="{{ route('zakat-fitrah.store') }}">
                @csrf                
                <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputPassword1" class="required">Date</label>
                        <input type="text" class="form-control" name="tanggal" id="datepicker" value="{{ $now }}" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1" class="required">Nama Muzaki</label>
                        <input type="text" class="form-control" id="nama" name="nama" value="" placeholder="masukkan nama muzaki" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1" class="required">Pilih Jenis Zakat</label>
                        <select name="jenis" id="jenis" class="form-control" required>
                            <option value="">-- Pilih Jenis Zakat --</option>
                            <option value="beras">Beras</option>
                            <option value="uang">Uang</option>
                        </select>
                    </div>
                    <div id="berasArea" style="display: none;">
                        <div class="form-group">
                            <label for="exampleInputPassword1" class="required">Jumlah (Kg)</label>
                            <input type="text" class="form-control" id="jumlah_beras" name="jumlah_beras" value="3">
                            <span class="required2">Gunakan tanda (.) untuk pemisah, misal 3.5kg cukup ditulis 3.5</span>
                        </div>
                    </div>
                    <div id="uangArea" style="display: none;">
                        <div class="form-group">
                            <label for="exampleInputPassword1" class="required">Jumlah Uang (rupiah)</label>
                            <input type="text" class="form-control" name="jumlah_uang" id="jumlah_uang" value="{{ old('harga_avg') ? old('harga_avg') : "36.000" }}">
                        </div>
                        <div class="form-group">
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" name="sudah_beli" class="custom-control-input parentPermission" data-kunci="1" id="sudah_beli" value="1" @if (old('sudah_beli') == '1')
                                    checked
                                @endif>
                                <label class="custom-control-label" for="sudah_beli" style="padding-top:3px;">Sudah Dibelikan Beras ?</label>
                            </div>
                        </div>     
                        <div class="form-group">
                            <label for="exampleInputPassword1" class="required">Sisa (Shodaqoh)</label>
                            <input type="text" class="form-control" name="sisa_uang" id="sisa_uang" value="{{ old('harga_avg') ? old('harga_avg') : 0 }}" readonly>
                        </div>
                    </div>
                    <div style="text-align:right;">
                        <a href="{{ route('zakat-fitrah.index') }}" class="btn btn-warning">Back</a>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>                
            </form>
        </div>
    </div>
</section>
@include ('includes.script')   
    @section('jsAdded')    
        <script src="https://unpkg.com/gijgo@1.9.14/js/gijgo.min.js" type="text/javascript"></script>
        <link href="https://unpkg.com/gijgo@1.9.14/css/gijgo.min.css" rel="stylesheet" type="text/css" />                
        <script>            
            $('#datepicker').datepicker({
                uiLibrary: 'bootstrap4'
            });

            $('#datepicker').datepicker({
                uiLibrary: 'bootstrap4'
            });

            $('#jumlah_beras').on('change, keyup', function() {
                var currentInput = $(this).val();
                var fixedInput = currentInput.replace(/[A-Za-z!,@#$%^&*()]/g, '');
                $(this).val(fixedInput);
            });

            jmlUang = null;
            uangInt = null;
            sisa = 0;
            $('#jumlah_uang').on('change, keyup', function() {
                jmlUang = $(this).val();
                jmlUang = jmlUang.replace(/[A-Za-z!,@#$%^&*()]/g, '');
                $(this).val(jmlUang);
                if (jmlUang == '') {
                    uangInt = 36000;                    
                    sisa = 0;
                    $("#sisa_uang").val(sisa.toLocaleString("id-ID"));
                    $("#jumlah_uang").val('');
                } else {                    
                    uangInt = jmlUang.replace('.', '')
                    uangInt = uangInt.replace(',', '.')                    
                    uangInt = parseInt(uangInt);
                    if (uangInt > 36000) {
                        sisa = uangInt - 36000;
                    } else if (uangInt <= 36000) {                        
                        sisa = 0;
                    }
                    $("#jumlah_uang").val(uangInt.toLocaleString("id-ID"));
                    $("#sisa_uang").val(sisa.toLocaleString("id-ID"));
                }                                                
            });
        
            let jenis = null;
            $(document).on('change','#jenis',function(){
                event.preventDefault();            
                jenis = $('#jenis option:selected').val();                
                if (jenis == 'beras') {
                    $("#berasArea").show();
                    $("#uangArea").hide();                    
                }else if(jenis == 'uang'){
                    $("#berasArea").hide();
                    $("#uangArea").show();
                }else {
                    $("#berasArea").hide();
                    $("#uangArea").hide();
                }
                $("#jumlah_beras").val(3);
                $("#jumlah_uang").val("36.000");
                $("#sisa_uang").val(0);
            });           

        </script>
    @endsection
@endsection
