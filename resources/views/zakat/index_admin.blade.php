@extends('layouts.app')

@section('title')
    <title>Halaman Zakat</title>
@endsection
@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h2 class="m-0 text-dark">Manajemen Zakat</h2>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item active">Zakat Fitrah</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<section class="container-fluid">    
    @include ('includes.flash')
    <div class="card shadow mb-4">
        <div class="card-body">  
            <div class="table-responsive">
                <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
                    <table id="data-admin" class="table table-bordered dataTable table-striped">
                        <thead>
                            <tr>
                                <th width="40">No</th>
                                <th>Kelompok Ronda</th>           
                                <th>Jumlah Muzaki</th>           
                                <th>Beras</th>                    
                                <th>Uang</th>                    
                                <th width="120">ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $key => $an)
                                <tr>
                                    <td class="text-center">{{$loop->iteration}}</td>
                                    <td>{{ $an->name }}</td>                                                        
                                    <td>
                                        {{ $an->jumlah_muzaki }} orang
                                    </td>                                                            
                                    <td>
                                        {{ $an->jumlah_beras }} orang
                                    </td>                                                            
                                    <td>
                                        {{ $an->jumlah_uang }} orang
                                    </td>                                                                                                                                                          
                                    <td class="text-center">
                                        @can('zakat-list')
                                            <a href="{{ route('zakat-fitrah.peronda',$an->id) }}">
                                                <button class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fa fa-eye"></i> Detail</button>
                                            </a>
                                        @endcan                                        
                                    </td>
                                </tr>                        
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
    @include('includes.modals_delete')
    @include ('includes.script')
    @can('zakat-create')
    @endcan
    @section('jsAdded')
        <script>
            let id = null;
            let url = null;
            let msg = null;
            let name = null;

            $(document).on('click','.btnDelete',function(){
                id = $(this).data('id');
                name = $(this).data('nama');
                url = "{{ route('zakat-fitrah.destroy', '') }}"+"/"+id;
                msg = "Apakah kamu yakin ingin menghapus zakat atas nama ("+name+") ?";
                $('#formDelete').attr('action', url);            
                $('#modalContent').empty();
                $('#modalContent').append(msg);
                $('#modalDelete').show();
            });

            $(document).on('click','.btnCancel',function(){            
                $('#modalDelete').hide();
            });
        </script>
    @endsection
@endsection
