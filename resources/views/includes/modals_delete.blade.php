{{-- modal --}}
<div id="modalDelete" class="modal" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form method="post" action="" id="formDelete">
            @csrf
            @method('DELETE')
            <div class="modal-header">
                <h5 class="modal-title">Delete Confirmation!</h5>
                <button type="button" class="close btnCancel" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">            
                <p id="modalContent">
      
                </p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary btnCancel" data-dismiss="modal">Close</button>          
                <button type="submit" class="btn btn-danger">Delete</button>
              </div>
        </form>        
      </div>
    </div>
</div>
{{-- end modal --}}