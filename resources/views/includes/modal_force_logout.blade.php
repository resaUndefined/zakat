{{-- modal --}}
<div id="modalForceLogout" class="modal" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form method="post" action="" id="formForceLogout">
            @csrf            
            <div class="modal-header">
                <h5 class="modal-title">Confirmation!</h5>
                <button type="button" class="close btnCancel" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">            
                <p id="modalContentLogout">
      
                </p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary btnCancel2" data-dismiss="modal">Close</button>          
                {{-- <a href="" class="btn btn-danger">Force Logout</a> --}}
                <button type="submit" class="btn btn-danger">Force Logout</button>
              </div>
        </form>        
      </div>
    </div>
</div>
{{-- end modal --}}