<ul class="navbar-nav bg-gradient-info sidebar sidebar-dark accordion" id="accordionSidebar">
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-180">
            <img class="rounded-circle" style="width: 30%;" src="{{ asset('img/mpm.jpeg') }}"> ZAKAT KEPUH
        </div>        
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="{{ route('home') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->    
    <!-- Heading -->    
    @if(Gate::check('permission-list') || Gate::check('role-list') || Gate::check('user-list'))
        <hr class="sidebar-divider">
        <div class="sidebar-heading">
            Credentials
        </div>
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseOne"
                aria-expanded="true" aria-controls="collapseOne">
                <i class="fas fa-fw fa-user-lock"></i>
                <span>Account Management</span>
            </a>
            <div id="collapseOne" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">            
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Account Management List:</h6>
                    @can('permission-list')
                        <a class="collapse-item" href="">Permissions</a>
                    @endcan
                    @can('role-list')
                        <a class="collapse-item" href="{{ route('roles.index') }}">Roles</a>
                    @endcan
                    @can('user-list')
                        <a class="collapse-item" href="{{ route('users.index') }}">Users</a>
                    @endcan
                </div>            
            </div>
        </li>    
    @endif    
    <hr class="sidebar-divider d-none d-md-block">
    <div class="sidebar-heading">
        Menu
    </div>
    <!-- Nav Item - Pages Collapse Menu -->
    @if(Gate::check('zakat-list'))
        <li class="nav-item">
            <a class="nav-link" href="{{ route('zakat-fitrah.index') }}">
            <i class="fas fa-fw fa-mosque"></i>
            <span>Zakat Fitrah</span></a>
        </li>
    @endif    
    <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="modal" data-target="#logoutModal">
            <i class="fas fa-fw fa-sign-out-alt"></i>
            <span>Logout</span></a>
        </a>        
    </li>    
    <!-- Divider -->
    {{-- <hr class="sidebar-divider d-none d-md-block"> --}}

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>