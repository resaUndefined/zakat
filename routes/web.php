<?php
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\ZakatController;
use App\Http\Controllers\Auth\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
  
Auth::routes();
  
Route::group(['middleware' => ['auth']], function() {
    Route::match(['GET','POST'],'logout', [LoginController::class, 'logout'])->name('logout');
    Route::get('/', [HomeController::class, 'index'])->name('home');
    Route::post('/force-logout/{id}', [UserController::class, 'force_logout'])->name('forceLogout');
    Route::post('/user-change-status/{id}', [UserController::class, 'change_status'])->name('changeStatus');
    Route::get('/zakat-fitrah/{id}/per-ronda', [ZakatController::class, 'index_peronda'])->name('zakat-fitrah.peronda');
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
    Route::resource('zakat-fitrah', ZakatController::class);
});
