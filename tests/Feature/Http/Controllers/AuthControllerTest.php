<?php

namespace Tests\Feature\Http\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Auth\RequestGuard;
use Illuminate\Support\Facades\Hash;

class AuthControllerTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function testRegisterUser()
    {
        $user = User::create([
            'name' => 'user test',
            'email' => 'user.test@gmail.com',
            'password' => Hash::make('123123'),
            'remember_token' => \Str::random(10),
            'role' => 'admin'
        ]);
        $credentials = [
            'email' => 'user.test@gmail.com',
            'password' => '123123',
            '_token' => csrf_token()
        ];
        $response = $this->call('POST', 'login', $credentials);
        $response = $this->actingAs($user)
                            ->withSession(['foo' => 'bar'])
                            ->get('/akun');
        $response->assertStatus(200);
        $checkUser = User::where('email','user.test@gmail.com')->get();
        $status = false;
        if (count($checkUser)) {
            $status = true;
        }
        $this->assertTrue($status);
    }

    public function testAccessHomeWithoutLogin()
    {
        $response = $this->get('/akun');

        $response->assertStatus(302);
    }

    public function testLoginGagal()
    {
        $response = $this->call('POST', 'login', [
            'email' => 'emailsalah@gmail.com',
            'password' => '123456',
            '_token' => csrf_token()
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/');
    }

    public function testLoginBerhasil()
    {
        $credentials = [
            'email' => 'user.test@gmail.com',
            'password' => '123123',
            '_token' => csrf_token()
        ];
        $response = $this->call('POST', 'login', $credentials);
        $response->assertRedirect('/');
    }

    public function testAccessHome()
    {
        $user = User::create([
            'name' => 'user test',
            'email' => 'user.test@gmail.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            'remember_token' => \Str::random(10),
            'role' => 'admin'
        ]);
        $response = $this->actingAs($user)
                            ->withSession(['foo' => 'bar'])
                            ->get('/akun');

        $response->assertStatus(200);
    }

    public function testChangePassword()
    {
        $user = User::create([
            'name' => 'user test',
            'email' => 'user.test@gmail.com',
            'password' => Hash::make('123123'),
            'remember_token' => \Str::random(10),
            'role' => 'admin'
        ]);
        $credentials = [
            'email' => 'user.test@gmail.com',
            'password' => '123123',
            '_token' => csrf_token()
        ];
        $response = $this->call('POST', 'login', $credentials);
        $status = false;
        $checkUser = User::where('email','user.test@gmail.com')->first();
        if (!is_null($checkUser)) {
            $status = true;
        }
        $this->assertTrue($status);
        $checkUser->password = Hash::make('qwe123');
        $checkUser->save();
        $this->assertTrue($status);
        $newCredentials = [
            'email' => 'user.test@gmail.com',
            'password' => 'qwe123',
            '_token' => csrf_token()
        ];
        $response = $this->call('POST', 'login', $newCredentials);
        $response->assertRedirect('/');
    }

    public function testLogout()
    {
        $user = User::create([
            'name' => 'user test',
            'email' => 'user.test@gmail.com',
            'password' => Hash::make('123123'),
            'remember_token' => \Str::random(10),
            'role' => 'admin'
        ]);
        $this->actingAs($user)
                            ->withSession(['foo' => 'bar'])
                            ->get('/akun');
        $this->get('/users');
        $this->assertTrue(session()->has('foo'));
        $this->get('/logout');
        session()->forget('foo');
        $this->assertFalse(session()->has('foo'));
    }

}
