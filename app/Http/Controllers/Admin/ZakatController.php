<?php

namespace App\Http\Controllers\Admin;

use App\Models\Zakat;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File; 
use DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class ZakatController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:zakat-list|zakat-create|zakat-edit|zakat-delete', ['only' => ['index','show']]);
         $this->middleware('permission:zakat-create', ['only' => ['create','store']]);
         $this->middleware('permission:zakat-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:zakat-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $roleName = $user->roles->pluck('name')[0];                
        if ($roleName == 'Admin' || $roleName == 'Takmir Masjid') {
            $data = User::role('Amil Zakat')->get();
            foreach ($data as $key => $value) {
                $uang = Zakat::where('user_id',$value->id)->where('jenis','uang')->count();
                $beras = Zakat::where('user_id',$value->id)->where('jenis','beras')->count();
                $value->jumlah_muzaki = $uang + $beras;
                $value->jumlah_uang = $uang;
                $value->jumlah_beras = $beras;
            }
            $this->data['data'] = $data;

            return view('zakat.index_admin',$this->data);
        } else {
            $data = Zakat::where('user_id',$user->id)->orderBy('tanggal','DESC')->get();
            $this->data['data'] = $data;        
            $this->data['amil'] = Auth::user();

            return view('zakat.index',$this->data);
        }                
    }

    public function index_peronda($id)
    {
        $user = User::findOrFail($id);
        $this->data['amil'] = $user;
        $this->data['data'] = Zakat::where('user_id',$user->id)->orderBy('tanggal','DESC')->get();

        return view('zakat.peronda',$this->data);
    }

    public function create()
    {                    
        $this->data['now'] = Carbon::now()->format('m/d/Y');

        return view('zakat.create', $this->data);
    }

    public function store(Request $request)
    {        
        $validator = Validator::make($request->all(), [
            'tanggal' => 'required',
            'nama' => 'required',
            'jenis' => 'required|IN:beras,uang',
            'jumlah_beras' => 'nullable',            
            'jumlah_uang' => 'nullable',            
            'sisa_uang' => 'nullable',            
            'sudah_beli' => 'nullable',            
        ]);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
        }      

        $zakat = new Zakat();
        $zakat->user_id = Auth::id();
        $zakat->nama = $request->nama;
        $split = explode('/',$request->tanggal);
        $zakat->tanggal = $split[2].'-'.$split[0].'-'.$split[1];
        $zakat->jenis = $request->jenis;
        if ($request->jenis == 'beras') {
            $zakat->jumlah_beras = $request->jumlah_beras;
            $zakat->jumlah_uang = null;
            $zakat->sisa_uang = null;
            $zakat->sudah_beli = 0;
        }else {
            $zakat->jumlah_beras = null;
            $zakat->jumlah_uang = '36000';
            if ($request->has('sudah_beli')) {
                $zakat->sudah_beli = 1;
            }else {
                $zakat->sudah_beli = 0;
            }    
            $sisa = str_replace(".","",$request->sisa_uang);
            $sisa = str_replace(",",".",$sisa);
            $zakat->sisa_uang = $sisa;            
        }
        $simpanZakat = $zakat->save();        
        if ($simpanZakat) {            
            toast('Zakat atas nama '.$request->nama.' berhasil disimpan!','success');

            return redirect()->route('zakat-fitrah.create');
        }else {
            toast('Zakat atas nama '.$request->nama.' gagal disimpan!','danger');            

            return back();
        }        
    }   

    public function edit($id)
    {
        $data = Zakat::findOrFail($id);
        $split = explode('-',$data->tanggal);
        $data->tanggal = $split[1].'/'.$split[2].'/'.$split[0];
        $this->data['data'] = $data;

        return view('zakat.edit', $this->data);
    }

    public function update(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'tanggal' => 'required',
            'nama' => 'required',
            'jenis' => 'required|IN:beras,uang',
            'jumlah_beras' => 'nullable',            
            'jumlah_uang' => 'nullable',            
            'sisa_uang' => 'nullable',            
        ]);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
        }      

        $zakat = Zakat::where('id',$id)->first();
        $zakat->user_id = Auth::id();
        $zakat->nama = $request->nama;
        $split = explode('/',$request->tanggal);
        $zakat->tanggal = $split[2].'-'.$split[0].'-'.$split[1];
        $zakat->jenis = $request->jenis;
        if ($request->jenis == 'beras') {
            $zakat->jumlah_beras = $request->jumlah_beras;
            $zakat->jumlah_uang = null;
            $zakat->sisa_uang = null;
            $zakat->sudah_beli = 0;
        }else {
            if ($request->has('sudah_beli')) {
                $zakat->sudah_beli = 1;
            }else {
                $zakat->sudah_beli = 0;
            }    
            $zakat->jumlah_beras = null;
            $zakat->jumlah_uang = '36000';
            $sisa = str_replace(".","",$request->sisa_uang);
            $sisa = str_replace(",",".",$sisa);
            $zakat->sisa_uang = $sisa;            
        }
        $simpanZakat = $zakat->save();        
        if ($simpanZakat) {            
            toast('Zakat atas nama '.$request->nama.' berhasil diupdate!','success');

            return redirect()->route('zakat-fitrah.index');
        }else {
            toast('Zakat atas nama '.$request->nama.' gagal diupdate!','danger');            

            return back();
        }  
    }

    public function show($id)
    {        
        $this->data['data'] = AjaibNews::findOrFail($id);
        
        return view('ajaib_news.show',$this->data);
    }

    public function destroy($id)
    {
        $zakat = Zakat::where('id',$id)->first();        
        if (!$zakat) {
            toast('Data tidak ditemukan!','danger');

            return redirect()->route('zakat-fitrah.index');
        }        
        $nama = $zakat->nama;
        $delete = $zakat->delete();
        if ($delete) {            
            toast('Data zakat atas nama '.$nama.' berhasil dihapus!','success');
        } else {
            toast('Data zakat atas nama '.$nama.' gagal dihapus!','danger');
        }        
        return redirect()->route('zakat-fitrah.index');
    }

}
