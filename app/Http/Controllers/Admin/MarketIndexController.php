<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Validator;
use App\Models\MarketIndex;

class MarketIndexController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:marketindex-list|marketindex-create|marketindex-edit|marketindex-delete', ['only' => ['index','show']]);
         $this->middleware('permission:marketindex-create', ['only' => ['create','store']]);
         $this->middleware('permission:marketindex-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:marketindex-delete', ['only' => ['destroy']]);
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['marketIndexs'] = MarketIndex::orderBy('created_at','DESC')->get();

        return view('market_index.index',$this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('market_index.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'nullable',
        ]);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
        }
    
        $marketIndex = MarketIndex::create($request->all());
        if ($marketIndex) {
            toast('Market Index created successfully!','success');
        } else {
            toast('Market Index failed to created!','danger');
        }
        
        return redirect()->route('market-index.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $marketIndex = MarketIndex::findOrFail($id);
        
        if (!$marketIndex) {
            toast('Market Index not found!','danger');

            return redirect()->route('market-index.index');
        }
        $this->data['marketIndex'] = $marketIndex;

        return view('market_index.show',$this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $marketIndex = MarketIndex::findOrFail($id);
        if (!$marketIndex) {
            toast('Market Index not found!','danger');

            return redirect()->route('market-index.index');
        }
        $this->data['marketIndex'] = $marketIndex;

        return view('market_index.edit',$this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'nullable',
        ]);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
        }

        $marketIndex = MarketIndex::where('id',$id)->first();
        if (!$marketIndex) {
            toast('Market Index not found!','danger');

            return redirect()->route('market-index.edit',$id);
        }
        $update = $marketIndex->update($request->all());
        if ($update) {
            toast('Market Index updated successfully!','success');

            return redirect()->route('market-index.index');
        } else {
            toast('Market Index failed to update!','danger');

            return redirect()->route('market-index.edit',$id);
        }        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $marketIndex = MarketIndex::where('id',$id)->first();
        if (!$marketIndex) {
            toast('Market Index not found!','danger');

            return redirect()->route('market-index.index');
        }
        $delete = $marketIndex->delete();
        if ($delete) {
            toast('Market Index deleted successfully!','success');
        } else {
            toast('Market Index failed to delete!','danger');
        }
        
        return redirect()->route('market-index.index');
    }
}
