<?php
    
namespace App\Http\Controllers\Admin;
    
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:user-list|user-create|user-edit|user-delete', ['only' => ['index','show']]);
         $this->middleware('permission:user-create', ['only' => ['create','store']]);
         $this->middleware('permission:user-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:user-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {        
        $data = User::where('id','!=',Auth::id())->orderBy('id','DESC')->get();
        
        return view('users.index',compact('data'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::where('name','!=','Admin')->pluck('name','id')->all();
        
        return view('users.create',compact('roles'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6|same:confirm-password',
            'roles' => 'required'
        ],[
            'password.same' => 'Password and password confirmation does not match!'
        ]);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
        }

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);                    
        $user = User::create($input);
        if ($user) {
            $user->assignRole($request->input('roles'));
            toast('User created successfully!','success');
        } else {
            toast('User failed to created!','danger');
        }        
    
        return redirect()->route('users.index');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        if (!$user) {
            toast('User not found!','danger');

            return redirect()->route('users.index');
        }

        return view('users.show',compact('user'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        if (!$user) {
            toast('User not found!','danger');

            return redirect()->route('users.index');
        }
        $roles = Role::where('name','!=','Admin')->pluck('name','id')->all();
        $userRole = $user->roles->pluck('id')->first();
        
        return view('users.edit',compact('user','roles','userRole'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'nullable|same:confirm-password',
            'roles' => 'required'
        ],[
            'password.same' => 'Password and password confirmation does not match!'
        ]);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
        }    
        $input = $request->all();
        if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = Arr::except($input,array('password'));    
        }
    
        $user = User::find($id);
        if (!$user) {
            toast('User not found!','danger');

            return redirect()->route('users.edit',$id);
        }
        $update = $user->update($input);
        if ($update) {
            DB::table('model_has_roles')->where('model_id',$id)->delete();
    
            $user->assignRole($request->input('roles'));
            toast('User updated successfully!','success');

            return redirect()->route('users.index');
        }else {
            toast('User failed to updated!','danger');

            return redirect()->route('users.edit',$id);
        }
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = User::find($id)->delete();
        if ($delete) {
            toast('User deleted successfully!','success');
        } else {
            toast('User failed to deleted!','danger');
        }
        
        return redirect()->route('users.index');
    }


    public function force_logout($id)
    {
        $user = User::find($id);
        if (!$user) {
            toast('User not found!','danger');

            return redirect()->route('users.index');
        }
        $name = $user->name;
        $user->logout = true;
        $save = $user->save();
        if ($save) {
            toast('User ('.$name.') force to logout successfully!','success');
        } else {
            toast('User ('.$name.') failed to force logout!','danger');
        }
        
        return redirect()->route('users.index');
    }

    public function change_status($id)
    {
        $user = User::find($id);
        if (!$user) {
            toast('User not found!','danger');

            return redirect()->route('users.index');
        }
        $name = $user->name;
        if ($user->is_active == 1) {
            $user->is_active = 0;
            $msgSuccess = 'User ('.$name.') deactivated successfully!';
            $msgFail = 'User ('.$name.') failed to deactivated!';
        } else {
            $user->is_active = 1;
            $msgSuccess = 'User ('.$name.') activated successfully!';
            $msgFail = 'User ('.$name.') failed to activated!';
        }                
        $save = $user->save();
        if ($save) {
            toast($msgSuccess,'success');
        } else {
            toast($msgFail,'danger');
        }
        
        return redirect()->route('users.index');
    }
}