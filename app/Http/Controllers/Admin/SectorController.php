<?php
    
namespace App\Http\Controllers\Admin;
    
use App\Models\Sector;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Validator;

class SectorController extends Controller
{ 
    function __construct()
    {
         $this->middleware('permission:sector-list|sector-create|sector-edit|sector-delete', ['only' => ['index','show']]);
         $this->middleware('permission:sector-create', ['only' => ['create','store']]);
         $this->middleware('permission:sector-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:sector-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['sectors'] = Sector::orderBy('created_at','DESC')->get();

        return view('sectors.index',$this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sectors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'nullable',
        ]);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
        }
    
        $sector = Sector::create($request->all());
        if ($sector) {
            toast('Sector created successfully!','success');
        } else {
            toast('Sector failed to created!','danger');
        }
        
        return redirect()->route('sectors.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sector = Sector::findOrFail($id);
        
        if (!$sector) {
            toast('Sector not found!','danger');

            return redirect()->route('sectors.index');
        }
        $this->data['sector'] = $sector;

        return view('sectors.show',$this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sector = Sector::findOrFail($id);
        if (!$sector) {
            toast('Sector not found!','danger');

            return redirect()->route('sectors.index');
        }
        $this->data['sector'] = $sector;

        return view('sectors.edit',$this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'nullable',
        ]);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
        }

        $sector = Sector::where('id',$id)->first();
        if (!$sector) {
            toast('Sector not found!','danger');

            return redirect()->route('sectors.edit',$id);
        }
        $update = $sector->update($request->all());
        if ($update) {
            toast('Sector updated successfully!','success');

            return redirect()->route('sectors.index');
        } else {
            toast('Sector failed to update!','danger');

            return redirect()->route('sectors.edit',$id);
        }        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sector = Sector::where('id',$id)->first();
        if (!$sector) {
            toast('Sector not found!','danger');

            return redirect()->route('sectors.index');
        }
        $delete = $sector->delete();
        if ($delete) {
            toast('Sector deleted successfully!','success');
        } else {
            toast('Sector failed to delete!','danger');
        }
        
        return redirect()->route('sectors.index');
    }
}