<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class RoleController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index','store']]);
         $this->middleware('permission:role-create', ['only' => ['create','store']]);
         $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $myRole = $user->roles()->pluck('id')->first();
        $this->data['roles'] = Role::where('id','!=',$myRole)->orderBy('id','DESC')->get();

        return view('roles.index',$this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parent = [
                        'role','user','zakat',
                    ];
        $permissionData = [];
        foreach ($parent as $key => $value) {
            $list = $value.'-list';
            $create = $value.'-create';
            $edit = $value.'-edit';
            $delete = $value.'-delete';
            $data = Permission::whereIn('name',[$list,$create,$edit,$delete])->orderBy('created_at','ASC')->get();
            $permissionData[$value] = $data;
        }        
        $this->data['permissions'] = $permissionData;
        
        return view('roles.create',$this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:roles,name',
            'permissions' => 'required',
        ]);
        
        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
        }
        
        $role = Role::create(['name' => $request->input('name')]);
        if ($role) {
            $role->syncPermissions($request->input('permissions'));            
            toast('Role created successfully!','success');
        }else {            
            toast('Role failed to created!','danger');
        }        
    
        return redirect()->route('roles.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find($id);
        if (!$role) {
            toast('Role failed to created!','danger');

            return redirect()->route('roles.index');
        }
        $this->data['role'] = $role;
        $this->data['rolePermissions'] = Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
                                                    ->where("role_has_permissions.role_id",$id)
                                                    ->get();
    
        return view('roles.show',$this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
        if (!$role) {
            toast('Role failed to created!','danger');

            return redirect()->route('roles.index');
        }
        $parent = [
                    'role','user','zakat',
                ];
        $permissionData = [];
        foreach ($parent as $key => $value) {
        $list = $value.'-list';
        $create = $value.'-create';
        $edit = $value.'-edit';
        $delete = $value.'-delete';
        $data = Permission::whereIn('name',[$list,$create,$edit,$delete])->orderBy('created_at','ASC')->get();
        $permissionData[$value] = $data;
        }        
        $this->data['permissions'] = $permissionData;
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
            ->all();
        
        $this->data['role'] = $role;        
        $this->data['rolePermissions'] = $rolePermissions;

        return view('roles.edit',$this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:roles,name,'.$id,
            'permissions' => 'required',
        ]);
        
        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
        }
    
        $role = Role::find($id);
        if (!$role) {
            toast('Role failed to created!','danger');

            return redirect()->route('roles.index');
        }
        $role->name = $request->input('name');
        $simpan = $role->save();
        if ($simpan) {
            $role->syncPermissions($request->input('permissions'));
            toast('Role updated successfully!','success');

            return redirect()->route('roles.index');
        } else {
            toast('Role failed to update!','danger');

            return redirect()->route('roles.edit',$id);
        }                    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = DB::table("roles")->where('id',$id)->delete();

        if ($delete) {
            toast('Role deleted successfully!','success');
        } else {
            toast('Role failed to delete!','danger');
        }        

        return redirect()->route('roles.index');
    }
}
