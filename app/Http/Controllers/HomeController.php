<?php

namespace App\Http\Controllers;

use App\Models\Zakat;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File; 
use DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {        
        $this->data['data'] = User::role('Amil Zakat')->orderBy('name')->get();        
        $zakatBeras = Zakat::where('jenis','beras')->count();
        $zakatUang = Zakat::where('jenis','uang')->count();
        $total = $zakatBeras + $zakatUang;        
        $berasPercentage = $zakatBeras/$total;
        $berasPercentage = round($berasPercentage, 2) * 100;
        $this->data['berasPercentage'] = (int)$berasPercentage;
        $uangPercentage = $zakatUang/$total;
        $uangPercentage = round($uangPercentage, 2) * 100;                
        $this->data['uangPercentage'] = (int)$uangPercentage;
        $this->data['zakatBeras'] = $zakatBeras;
        $this->data['zakatUang'] = $zakatUang;
        $user = Auth::user();
        $roleName = $user->roles->pluck('name')[0];
        $this->data['roleName'] = $roleName;
        $this->data['totalBeras'] = Zakat::where('jenis','beras')->sum('jumlah_beras');
        $this->data['totalUang'] = Zakat::where('jenis','uang')->where('sudah_beli',0)->sum('jumlah_uang');
        $this->data['totalUangBeras'] = Zakat::where('jenis','uang')->where('sudah_beli',1)->sum('jumlah_uang');
        $this->data['totalShodaqoh'] = Zakat::where('jenis','uang')->sum('sisa_uang');
        $this->data['konversiUang'] = $zakatUang * 3;
        return view('home', $this->data);
    }
}
