<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use DB;
use App\Models\User;
use RealRashid\SweetAlert\Facades\Alert;
use Session;
use Hash;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{    
    protected $maxAttempts = 5;
    protected $decayMinutes = 2;    

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);

        $checkUser = User::where(['email' => $request->email,'is_active' => 1])->first();
        if (!$checkUser) {
            Alert::warning('Warning', 'Pengguna tidak aktif!');
            
            return redirect()->route('login');
        }
             
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            $checkUser->logout = false;
            $checkUser->save();
            $this->clearLoginAttempts($request);
            toast('Selamat Datang Kembali '.$checkUser->name.'!','success');            

            return redirect()->route('home');
        }else {
            $this->incrementLoginAttempts($request);
            Alert::warning('Warning', 'Opps! Email atau password kamu salah.');
        
            return redirect()->route('login');
        }        
    }

    public function logout() {
        Session::flush();
        $user = User::where('id',Auth::id())->first();
        $user->logout = true;
        $user->save();
        Auth::logout();

        return redirect()->route('login');
    }
    
}
