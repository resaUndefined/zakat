<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;

class LogoutUsers
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if (! empty($user->logout)) {  
            if ($user->logout == true) {
                Auth::logout();
                return redirect()->route('login');
            } else {
                return $next($request);
            }            
        }

        return $next($request);
    }
}
