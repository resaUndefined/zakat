<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Zakat extends Model
{
    use HasFactory;

    protected $table = 'zakat';
    protected $fillable = [        
        'nama',
        'tanggal',        
        'user_id',        
        'jenis',        
        'jumlah_beras',        
        'jumlah_uang',        
        'sisa_uang',        
        'sudah_beli',        
    ];  

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
