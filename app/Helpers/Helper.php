<?php

function matrikSkala($n){
    $m_skala = [];
    $m_skala[1] = 0;
    $m_skala[2] = 0;
    $m_skala[3] = 0.58;
    $m_skala[4] = 0.9;
    $m_skala[5] = 1.12;
    $m_skala[6] = 1.24;
    $m_skala[7] = 1.32;
    $m_skala[8] = 1.41;
    $m_skala[9] = 1.46;
    $m_skala[10] = 1.49;
    $m_skala[11] = 1.51;
    $m_skala[12] = 1.48;
    $m_skala[13] = 1.56;
    $m_skala[14] = 1.57;
    $m_skala[15] = 1.59;
    if($n == 'all'){
        return $m_skala;
    }else if(!$n){
        return null;
    }
    return $m_skala[$n];
}

function prioritas(){
    $data = array();
    for($x = 1; $x <= 9; $x++){
        $data[] = $x;
        if($x!==1){
            $data[] = round(1 / $x, 2);
        }
    }

    return collect($data)->sort();
}
