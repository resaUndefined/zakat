<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionAddedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'user-list',
            'user-create',
            'user-edit',
            'user-delete',
            'emiten-list',
            'emiten-create',
            'emiten-edit',
            'emiten-delete',
            'marketindex-list',
            'marketindex-create',
            'marketindex-edit',
            'marketindex-delete',
            'modal-list',
            'modal-create',
            'modal-edit',
            'modal-delete',
            'detailmodal-list',
            'detailmodal-create',
            'detailmodal-edit',
            'detailmodal-delete',
            'transaksiharian-list',
            'transaksiharian-create',
            'transaksiharian-edit',
            'transaksiharian-delete',
            'transaksibulanan-list',
            'transaksibulanan-create',
            'transaksibulanan-edit',
            'transaksibulanan-delete',
            'transaksitahunan-list',
            'transaksitahunan-create',
            'transaksitahunan-edit',
            'transaksitahunan-delete',
         ];
      
         foreach ($permissions as $permission) {
              Permission::create(['name' => $permission]);
         }
    }
}
