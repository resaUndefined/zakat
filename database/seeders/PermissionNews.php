<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionNews extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'news-list',
            'news-create',
            'news-edit',
            'news-delete',            
            'helpdesk-list',
            'helpdesk-create',
            'helpdesk-edit',
            'helpdesk-delete',            
         ];
      
         foreach ($permissions as $permission) {
              Permission::create(['name' => $permission]);
         }
    }
}
